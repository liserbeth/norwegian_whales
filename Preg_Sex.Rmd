---
title: "HW_pregnancy_sex"
author: "Lisa Elena Kettemer"
date: "3/10/2022"
output: html_document
---

```{r setup, include=FALSE}
#knitr::opts_chunk$set(echo = TRUE)
require(datapasta)
require(tidyverse)
require(data.table)
require(wesanderson)
require(janitor)
```


```{r load}
df2 <- readRDS( "data/tidy/Sex_HW.RData")
df_full <- readRDS("data/raw/Overview_HW_biopsies.RData")
```


```{r data}
#results from model assigning likelihood of pregnancy based on progesterone concentration in blubber
df<-tibble::tribble(
        ~Sample, ~Extraction,     ~CCGL.ID, ~Sample.Number, ~Year,        ~Date, ~Julian.Day,         ~P4,   ~`log(P4)`, ~Preg, ~Pregnant, ~Year, ~Month, ~Season,           ~Notes, ~LH.Stage, ~LH.Class, ~Percent.Lipid, ~Probabil,      ~Hi, ~Hi.Correct,     ~Low, ~Low.Correct,
  "2016.01.001",          NA,           NA,             NA, 2016L, "12/31/2016",          NA, 127.6640883,  2.106068748,    1L,     "Yes", 2016L,    12L, "16/17", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2016.01.006",          NA,           NA,             NA, 2016L,  "11/7/2016",          NA, 102.6236327,  2.011247384,    1L,     "Yes", 2016L,    11L, "16/17", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2016.01.019",          NA,           NA,             NA, 2016L,   "2/6/2016",          NA, 1.311666243,  0.117823342,    0L,      "No", 2016L,     2L, "15/16", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2017.01.001",          NA,           NA,             NA, 2017L, "11/14/2017",          NA, 0.706203896, -0.151069891,    0L,      "No", 2017L,    11L, "17/18", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2017.01.005",          NA,           NA,             NA, 2017L, "11/14/2017",          NA, 0.417354288, -0.379495121,    0L,      "No", 2017L,    11L, "17/18", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2017.01.006",          NA,           NA,             NA, 2017L, "11/14/2017",          NA, 0.843229911, -0.074053997,    0L,      "No", 2017L,    11L, "17/18", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2017.01.007",          NA,           NA,             NA, 2017L,   "1/4/2017",          NA,  1.10359132,  0.042808276,    0L,      "No", 2017L,     1L, "16/17", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2017.01.008",          NA,           NA,             NA, 2017L,   "1/4/2017",          NA, 1.315398485,  0.119057337,    0L,      "No", 2017L,     1L, "16/17", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2017.01.010",          NA,           NA,             NA, 2017L,  "1/15/2017",          NA, 164.7148019,  2.216732628,    1L,     "Yes", 2017L,     1L, "16/17", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2017.01.011",          NA,           NA,             NA, 2017L,  "1/13/2017",          NA, 1.201915112,  0.079873796,    0L,      "No", 2017L,     1L, "16/17", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2017.01.012",          NA,           NA,             NA, 2017L,  "1/13/2017",          NA, 0.578473709, -0.237716375,    0L,      "No", 2017L,     1L, "16/17", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2017.01.015",          NA,           NA,             NA, 2017L,  "1/11/2017",          NA, 6.773915265,   0.83083976,    0L,      "No", 2017L,     1L, "16/17", "Coastal Norway",        NA,        NA,             NA,  2.47e-15, 1.81e-06,  "1.81E-06", 2.22e-16,   "2.25E-15",
  "2018.01.001",          NA,           NA,             NA, 2018L,   "9/4/2018",          NA, 0.718404075, -0.143631213,    0L,      "No", 2018L,     9L,    "18",    "Barents Sea",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.002",          NA,           NA,             NA, 2018L,   "9/4/2018",          NA, 1.109227287,  0.045020544,    0L,      "No", 2018L,     9L,    "18",    "Barents Sea",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.003",          NA,           NA,             NA, 2018L,   "9/4/2018",          NA, 303.7914581,  2.482575558,    1L,     "Yes", 2018L,     9L,    "18",    "Barents Sea",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2018.01.004",          NA,           NA,             NA, 2018L,   "9/4/2018",          NA, 194.7449783,  2.289466268,    1L,     "Yes", 2018L,     9L,    "18",    "Barents Sea",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2018.01.005",          NA,           NA,             NA, 2018L,   "9/4/2018",          NA, 0.882724529, -0.054174805,    0L,      "No", 2018L,     9L,    "18",    "Barents Sea",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.009",          NA,           NA,             NA, 2018L,   "9/7/2018",          NA, 0.312042263, -0.505786581,    0L,      "No", 2018L,     9L,    "18",    "Barents Sea",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.015",          NA,           NA,             NA, 2018L,   "9/7/2018",          NA, 0.372677288, -0.428667073,    0L,      "No", 2018L,     9L,    "18",    "Barents Sea",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.016",          NA,           NA,             NA, 2018L,   "9/7/2018",          NA, 0.486273738, -0.313119185,    0L,      "No", 2018L,     9L,    "18",    "Barents Sea",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.017",          NA,           NA,             NA, 2018L,   "9/8/2018",          NA, 0.437398231, -0.359122978,    0L,      "No", 2018L,     9L,    "18",    "Barents Sea",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.018",          NA,           NA,             NA, 2018L,   "9/8/2018",          NA, 0.482736622, -0.316289753,    0L,      "No", 2018L,     9L,    "18",    "Barents Sea",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.020",          NA,           NA,             NA, 2018L,   "9/8/2018",          NA,  0.32306122, -0.490715171,    0L,      "No", 2018L,     9L,    "18",    "Barents Sea",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.022",          NA,           NA,             NA, 2018L,   "9/9/2018",          NA, 0.324596969,  -0.48865554,    0L,      "No", 2018L,     9L,    "18",    "Barents Sea",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.023",          NA,           NA,             NA, 2018L,   "9/9/2018",          NA, 0.168455488, -0.773514836,    0L,      "No", 2018L,     9L,    "18",    "Barents Sea",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.026",          NA,           NA,             NA, 2018L,   "9/9/2018",          NA,  0.20848949, -0.680915833,    0L,      "No", 2018L,     9L,    "18",    "Barents Sea",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.028",          NA,           NA,             NA, 2018L,   "9/9/2018",          NA, 0.567278686, -0.246203534,    0L,      "No", 2018L,     9L,    "18",    "Barents Sea",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.032",          NA,           NA,             NA, 2018L, "10/26/2018",          NA, 0.520011923, -0.283986699,    0L,      "No", 2018L,    10L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.033",          NA,           NA,             NA, 2018L, "10/26/2018",          NA, 0.609093895, -0.215315753,    0L,      "No", 2018L,    10L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.038",          NA,           NA,             NA, 2018L,  "11/6/2018",          NA, 347.9784932,  2.541552403,    1L,     "Yes", 2018L,    11L, "18/19", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2018.01.039",          NA,           NA,             NA, 2018L,  "11/6/2018",          NA,  0.91413743, -0.038988508,    0L,      "No", 2018L,    11L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.040",          NA,           NA,             NA, 2018L,  "11/6/2018",          NA, 0.456686488, -0.340381838,    0L,      "No", 2018L,    11L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.041",          NA,           NA,             NA, 2018L,  "11/6/2018",          NA, 0.510225088, -0.292238191,    0L,      "No", 2018L,    11L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.043",          NA,           NA,             NA, 2018L,  "11/7/2018",          NA, 1.843337596,  0.265604881,    0L,      "No", 2018L,    11L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.044",          NA,           NA,             NA, 2018L,  "11/7/2018",          NA, 79.58533376,  1.900833042,    1L,     "Yes", 2018L,    11L, "18/19", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2018.01.045",          NA,           NA,             NA, 2018L,  "11/7/2018",          NA, 153.6396624,  2.186503344,    1L,     "Yes", 2018L,    11L, "18/19", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2018.01.046",          NA,           NA,             NA, 2018L,  "11/7/2018",          NA, 1.933410686,  0.286324115,    0L,      "No", 2018L,    11L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.047",          NA,           NA,             NA, 2018L,  "11/7/2018",          NA, 261.1418481,  2.416876473,    1L,     "Yes", 2018L,    11L, "18/19", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2018.01.048",          NA,           NA,             NA, 2018L, "11/13/2018",          NA, 0.655320773, -0.183546065,    0L,      "No", 2018L,    11L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.049",          NA,           NA,             NA, 2018L, "11/13/2018",          NA, 0.383548177, -0.416180077,    0L,      "No", 2018L,    11L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.051",          NA,           NA,             NA, 2018L, "11/15/2018",          NA, 872.4925177,  2.940761711,    1L,     "Yes", 2018L,    11L, "18/19", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2018.01.053",          NA,           NA,             NA, 2018L,  "12/2/2018",          NA, 190.3375529,  2.279524482,    1L,     "Yes", 2018L,    12L, "18/19", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2018.01.054",          NA,           NA,             NA, 2018L,  "12/2/2018",          NA, 0.811878619, -0.090508896,    0L,      "No", 2018L,    12L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.057",          NA,           NA,             NA, 2018L,  "12/4/2018",          NA, 136.0270604,  2.133625313,    1L,     "Yes", 2018L,    12L, "18/19", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2018.01.058",          NA,           NA,             NA, 2018L,  "12/4/2018",          NA, 106.6856462,  2.028105992,    1L,     "Yes", 2018L,    12L, "18/19", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2018.01.059",          NA,           NA,             NA, 2018L,  "12/4/2018",          NA, 0.925390389, -0.033675016,    0L,      "No", 2018L,    12L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.060",          NA,           NA,             NA, 2018L,  "12/4/2018",          NA, 84.05189925,  1.924547531,    1L,     "Yes", 2018L,    12L, "18/19", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2018.01.061",          NA,           NA,             NA, 2018L,  "12/4/2018",          NA, 0.259553442, -0.585773208,    0L,      "No", 2018L,    12L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2018.01.063",          NA,           NA,             NA, 2018L,  "12/4/2018",          NA,  287.296901,  2.458330941,    1L,     "Yes", 2018L,    12L, "18/19", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2019.01.001",          NA,           NA,             NA, 2019L,   "1/8/2019",          NA, 196.0461137,  2.292358237,    1L,     "Yes", 2019L,     1L, "18/19", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2019.01.002",          NA,           NA,             NA, 2019L,   "1/8/2019",          NA, 97.59533626,  1.989429065,    1L,     "Yes", 2019L,     1L, "18/19", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2019.01.003",          NA,           NA,             NA, 2019L,   "1/8/2019",          NA, 0.317329908, -0.498488995,    0L,      "No", 2019L,     1L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2019.01.004",          NA,           NA,             NA, 2019L,   "1/9/2019",          NA, 0.844003815,  -0.07365559,    0L,      "No", 2019L,     1L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2019.01.005",          NA,           NA,             NA, 2019L,   "1/9/2019",          NA, 322.5546217,  2.508603269,    1L,     "Yes", 2019L,     1L, "18/19", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2019.01.008",          NA,           NA,             NA, 2019L,   "1/9/2019",          NA, 1.582404165,  0.199317417,    0L,      "No", 2019L,     1L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2019.01.009",          NA,           NA,             NA, 2019L,  "1/19/2019",          NA, 260.4045613,  2.415648587,    1L,     "Yes", 2019L,     1L, "18/19", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2019.01.014",          NA,           NA,             NA, 2019L,  "1/19/2019",          NA, 0.961315948, -0.017133853,    0L,      "No", 2019L,     1L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2019.01.016",          NA,           NA,             NA, 2019L,  "1/23/2019",          NA, 0.503806297, -0.297736409,    0L,      "No", 2019L,     1L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2019.01.017",          NA,           NA,             NA, 2019L,  "1/24/2019",          NA, 0.372110462,  -0.42932812,    0L,      "No", 2019L,     1L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2019.01.019",          NA,           NA,             NA, 2019L,  "1/24/2019",          NA, 0.545209432, -0.263436639,    0L,      "No", 2019L,     1L, "18/19", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2020.01.001",          NA,           NA,             NA, 2020L,   "1/6/2020",          NA, 1.763088392,  0.246274086,    0L,      "No", 2020L,     1L, "19/20", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2016.01.024",          NA, "Mnova16002",             NA, 2016L,  "1/13/2016",          NA, 0.956803476, -0.019177256,    0L,      "No", 2016L,     1L, "15/16", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2016.01.008",          NA, "Mnova16003",             NA, 2016L,   "2/1/2016",          NA, 0.557348336, -0.253873291,    0L,      "No", 2016L,     2L, "15/16", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2016.01.011",          NA, "Mnova16006",             NA, 2016L,   "2/1/2016",          NA, 0.593032505, -0.226921502,    0L,      "No", 2016L,     2L, "15/16", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2016.01.015",          NA, "Mnova16010",             NA, 2016L,   "2/6/2016",          NA, 198.8373512,  2.298497969,    1L,     "Yes", 2016L,     2L, "15/16", "Coastal Norway",        NA,        NA,             NA,         1,        1,     "0E+00",        1,      "0E+00",
  "2016.01.017",          NA, "Mnova16012",             NA, 2016L,   "2/6/2016",          NA, 0.622019928, -0.206195702,    0L,      "No", 2016L,     2L, "15/16", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2016.01.019",          NA, "Mnova16014",             NA, 2016L,   "2/6/2016",          NA, 0.652813886, -0.185210617,    0L,      "No", 2016L,     2L, "15/16", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00",
  "2016.01.021",          NA, "Mnova16016",             NA, 2016L,  "2/20/2016",          NA,  0.69174374, -0.160054762,    0L,      "No", 2016L,     2L, "15/16", "Coastal Norway",        NA,        NA,             NA,  2.22e-16, 2.22e-16,     "0E+00", 2.22e-16,      "0E+00"
  )

```


```{r scottish_data}
#adding J.Kershaw's data
#we confirmed that the extraction method and assigning method have produced the same result for 13 individuals
#despite the slightly different methods in extraction we can thus combine datasets
df3<- tibble::tribble(
             ~Area, ~Year,      ~Date,  ~Sample.ID,                                ~Tag.Record,                   ~Animal.ID,     ~Sex, ~Age.Class,             ~Comments, ~`Progesterone.(ng/g)`,  ~Pregnant, ~`Hormone.sample.(g)`,
  "Coastal Norway", 2011L,   "6/6/11",      "o135",                                "Mn11_157a",                           NA, "Female",    "Adult",                    NA,                  30.78,      "Yes",                0.1707,
  "Coastal Norway", 2011L,  "6/15/11",      "o235",                                "Mn11_165e",                           NA,   "Male",    "Adult",                    NA,                     NA,         NA,                0.1666,
  "Coastal Norway", 2011L,  "6/15/11",      "o335",                                         NA,                           NA, "Female",    "Adult",                    NA,                   3.64,         NA,                0.1904,
  "Coastal Norway", 2012L,  "1/21/12",      "o535",                                         NA,                           NA,   "Male",         NA,                    NA,                     NA,         NA,                0.1753,
  "Coastal Norway", 2012L,  "1/21/12",      "o235",                                         NA,                           NA,   "Male",         NA,           "skin only",                     NA,         NA,                    NA,
  "Coastal Norway", 2012L,  "1/21/12",      "o335",                                         NA,                           NA,   "Male",         NA,                    NA,                   3.42,         NA,                0.1579,
  "Coastal Norway", 2012L,  "1/21/12",      "o435",                                         NA,                           NA,   "Male",         NA,                    NA,                   3.27,         NA,                0.0905,
  "Coastal Norway", 2012L,  "1/31/12",      "o835",                                         NA,                           NA,   "Male",         NA,           "skin only",                     NA,         NA,                    NA,
  "Coastal Norway", 2012L,  "3/17/12",      "o935",                                         NA,                           NA,   "Male",         NA,           "skin only",                     NA,         NA,                    NA,
  "Coastal Norway", 2012L,  "6/10/12",      "o235", "Mn12_161a/b (2 tag records, same animal)",                           NA,   "Male",    "Adult",                    NA,                     NA,         NA,                0.1457,
  "Coastal Norway", 2012L,  "6/13/12",      "o335",                                  "Mn164 A",                           NA, "Female",    "Adult",                    NA,                   3.49,         NA,                0.1792,
  "Coastal Norway", 2012L,  "6/13/12",      "o435",                                  "Mn164 B",                           NA, "Female", "Juvenile",                    NA,                   3.23,         NA,                0.0752,
  "Coastal Norway", 2012L,  "6/18/12",      "o735",                                  "Mn170 A",                           NA, "Female", "Juvenile", "c/c pair potential?",                   3.42,         NA,                0.1572,
  "Coastal Norway", 2012L,  "6/18/12",      "o635",                                  "Mn170 B",                           NA, "Female",    "Adult", "c/c pair potential?",                  15.67,      "Yes",                0.1542,
  "Coastal Norway", 2012L,  "6/20/12",      "o835",    "Mn171 AB (2 tag records, same animal)",                           NA,   "Male",    "Adult",                    NA,                     NA,         NA,                0.1962,
  "Coastal Norway", 2012L,  "6/25/12",      "o935",                                 "Mn 178 A",                           NA, "Female",    "Adult",                    NA,                   3.63,         NA,                 0.174,
  "Coastal Norway", 2012L,  "6/25/12",      "1035",                                         NA,        "follower of Mn178 A", "Female",    "Adult",                    NA,                   6.67, "Perhaps?",                0.1622,
  "Coastal Norway", 2012L,  "6/29/12",      "1235",                                         NA, "calf / follow of Mn 180 AB", "Female",     "Calf",                "calf",                   3.49,         NA,                0.1639,
  "Coastal Norway", 2012L,  "6/29/12",      "1135",    "Mn180 AB (2 tag records, same animal)",                           NA, "Female",    "Adult",                 "Cow",                   4.93,         NA,                0.1965,
  "Coastal Norway", 2013L, "11/29/13",      "o135",                        "2_Mn_2013_Kvaløya",                           NA, "Female",         NA,                    NA,                   3.53,         NA,                0.1879,
  "Coastal Norway", 2013L, "11/30/13",      "o235",                        "3_Mn_2013_Kvaløya",                           NA,   "Male",         NA,                    NA,                     NA,         NA,                0.1225,
  "Coastal Norway", 2013L,  "12/5/13",      "o635",                        "7_Mn_2013_Kvaløya",                           NA,   "Male",         NA,                    NA,                     NA,         NA,                0.1924,
  "Coastal Norway", 2013L,  "12/6/13",      "o735",                                         NA,                           NA, "Female",         NA,                    NA,                  10.46,      "Yes",                0.0858,
  "Coastal Norway", 2013L,  "12/7/13",      "o835",                                         NA,                           NA,   "Male",         NA,                    NA,                     NA,         NA,                0.1857,
  "Coastal Norway", 2013L,  "12/7/13",      "1035",                                         NA,                           NA, "Female",         NA,                    NA,                  12.31,      "Yes",                0.2001,
  "Coastal Norway", 2014L,  "1/26/14",      "o335",                                         NA,                           NA,   "Male",         NA,                    NA,                     NA,         NA,                0.1866,
  "Coastal Norway", 2014L,  "1/26/14",      "o135",                                         NA,                           NA, "Female",         NA,                    NA,                    3.6,         NA,                0.1927,
  "Coastal Norway", 2014L,  "1/26/14",      "o235",                                         NA,                           NA, "Female",         NA,                    NA,                   3.52,         NA,                0.1875,
  "Coastal Norway", 2014L,  "1/30/14",      "o435",                                         NA,                           NA,   "Male",         NA,                    NA,                     NA,         NA,                0.1572,
  "Coastal Norway", 2014L,  "1/31/14",      "o635",                                         NA,                           NA, "Female",         NA,                    NA,                   3.47,         NA,                0.1715,
  "Coastal Norway", 2014L,  "1/31/14",      "o735",                                         NA,                           NA, "Female",         NA,                    NA,                   3.39,         NA,                0.1582,
  "Coastal Norway", 2016L,  "1/17/16",  "LK-Mn-01",                                "Mn16_017a",                           NA,   "Male",    "Adult",                    NA,                     NA,         NA,                 0.193,
  "Coastal Norway", 2016L,  "1/18/16",  "LK-Mn-02",                                "Mn16_018a",                           NA,   "Male",    "Adult",                    NA,                     NA,         NA,                0.1807,
  "Coastal Norway", 2016L,  "1/19/16",  "TI-Mn-01",                              "Mn16_Jan19a",                           NA,   "Male",    "Adult",           "skin only",                     NA,         NA,                    NA,
  "Coastal Norway", 2016L,  "1/20/16",  "TI-Mn-02",                                         NA,                           NA, "Female",    "Adult",           "skin only",                     NA,         NA,                    NA,
  "Coastal Norway", 2016L,  "1/20/16",  "LK-Mn-03",                                "Mn16_020a",                           NA,   "Male",    "Adult",                    NA,                     NA,         NA,                0.1148,
  "Coastal Norway", 2016L,  "1/20/16",  "LK-Mn-04",                     "HV Tag B (Lars' Tag)",                           NA, "Female",    "Adult",                    NA,                   3.42,         NA,                0.1246,
  "Coastal Norway", 2016L,  "1/21/16",  "CH-Mn-05",                               "Mn16_Jan21",                           NA, "Female",    "Adult",                    NA,                   2.56,         NA,                0.1401,
  "Coastal Norway", 2016L,  "1/23/16",  "SI-Mn-06",                                "Mn16_023a",                           NA,   "Male",    "Adult",                    NA,                     NA,         NA,                0.1304,
  "Coastal Norway", 2016L,  "1/24/16",  "LK-Mn-07",                                "Mn16_024a",                           NA, "Female",    "Adult",                    NA,                    2.6,         NA,                0.0685,
  "Coastal Norway", 2016L,  "1/25/16",  "LK-Mn-08",                                "Mn16_025b",                           NA,   "Male",    "Adult",           "skin only",                     NA,         NA,                    NA,
  "Coastal Norway", 2016L,  "1/25/16",  "LK-Mn-09",                     "HV Tag B (Lars' Tag)",                           NA, "Female",    "Adult",                    NA,                   2.65,         NA,                 0.161,
  "Coastal Norway", 2016L,  "1/26/16",  "CH-Mn-10",                               "Mn16_Jan26",                           NA, "Female",    "Adult",                    NA,                   2.72,         NA,                0.1687,
  "Coastal Norway", 2017L,  "1/18/17",  "LK-Mn-01",                                "Mn17_018a",                           NA,   "Male",    "Adult",                    NA,                     NA,         NA,                0.1571,
  "Coastal Norway", 2017L,  "1/22/17",  "MW-Mn-05",                                "Mn17_022b",                           NA, "Female",    "Adult",                    NA,                   8.64, "Perhaps?",                0.1508,
  "Coastal Norway", 2017L,  "1/22/17",  "LK-Mn-04",                                "Mn17_022a",                           NA, "Female",    "Adult",                    NA,                  30.76,      "Yes",                0.1713,
  "Coastal Norway", 2017L,  "1/22/17",  "LK-Mn-03",                                 "Lost Tag",                           NA, "Female",    "Adult",                    NA,                   2.84,         NA,                0.1917,
  "Coastal Norway", 2017L,  "1/24/17", "LK-Mn-06b",                                "Mn17_024a",                           NA,   "Male",    "Adult",                    NA,                     NA,         NA,                0.1489,
  "Coastal Norway", 2017L,  "1/26/17",  "MB-Mn-07",                              "Mn17_026lla",                           NA, "Female",    "Adult",                    NA,                  23.77,      "Yes",                0.1741
  )


```



```{r tidy}
##Jo's prgnancy results table
janitor::clean_names(df3)-> df3
#--> use all females and the males for sex ratio
sex_Jo <- df3
#remove male controls from preg df
df3$control <- ifelse(df3$sex=="Male","1","0" )
df3 <- filter(df3, control == 0)
#Replace "NA" in pregnant with "No" if hormones were extracted
ifelse(!is.na(df3$hormone_sample_g) & is.na(df3$pregnant), "No" , df3$pregnant) -> df3$pregnant
#remove the "maybe" pregnant individual and "NA"
filter(df3, !pregnant== "Perhaps?") -> df3
# we got 27 individual females assessed by Jo; 2 not assigned, 1 no blubber available, 
#24 for preg rate
#Deal with dates
names(df3)
df3$date <- lubridate::mdy(df3$date)
julian(df3$date)->df3$julian
#Assign Season 
assign_season<- function(date){
  month <- month(date)
  year <- year(date)
  temp <- ifelse(month %in% c(1,2,3),"early", NA)
  temp <- ifelse(month %in% c(4,5,6,7,8,9),"mid", temp)
  temp <- ifelse(month %in% c(10,11,12),"late", temp)

  season <- ifelse(temp=="early",paste(year-1,"/",year), NA)
  season <- ifelse(temp=="mid",paste(year), season)
  season <- ifelse(temp=="late",paste(year,"/",year+1), season)
  return(season)
  }

df3$season <- assign_season(df3$date)
df3$month <- month(df3$date)
df3$year <- year(df3$date)
##select required columns
#harmonize columns
df3 <- select(df3,1:4,11,13,14:16)
df3$preg <- ifelse(df3$pregnant == "Yes", 1, 0)

###Own pregnancy results table
names(df)
janitor::clean_names(df) -> df
df$date %>% lubridate::mdy()-> df$date
df$julian <- julian(df$date)
df$season <- assign_season(df$date)
df$month <- month(df$date)
df$year <- year(df$date)
#remove duplicate which was run twice through the model with similar prog. concentrations (it was extracted multiple times and run through the model twice)
df<-df[-(which(duplicated(df$sample))),]

#clean sex table
clean_names(df2) -> df2

##get sex into pregnancy table from sex table
left_join(df,df2) -> df
df$control <- ifelse(df$sex=="M","1","0")
#remove males
df<- filter(df,control==0)

###Overview table of all samples
janitor::clean_names(df_full)-> df_full
df_full$date %>% as.Date("%m/%d/%y") -> df_full$date
julian(df_full$date)->df_full$julian
df_full$month <- month(df_full$date)
df_full$year <- year(df_full$date)
df_full$season <- assign_season(df_full$date)

```


```{r merge_dfs}
#harmonize column names
rename(df3,sample=sample_id, notes=area)->df3
select(df,"sample","year","date","preg","month","season","notes","julian","control") -> df4
select(df3,!pregnant) %>% rbind(.,df4) -> df_preg
#remove single individual assessed in 2020 to check whether a specific female sighted with calf was pregnant again
df_preg[!df_preg$year=="2020",]-> df_preg
#order months so that the table is usefully ordered
df_preg$month <- factor(df_preg$month,levels = c("6","9", "10", "11", "12","1","2"))

```


```{r pregnancy_tables}
dt <- setDT(df_preg)
dt$month <- ordered(dt$month,levels = c("6","9", "10", "11", "12","1","2"))

#includes BS samples and summer samples, no controls, no NAs, and no undetermined samples
dt[,.(.N,N_preg=length(which(preg==1)),N_not_preg=length(which(preg==0))), by=season][,prop_preg:=round(N_preg/N,2)]->preg_season   #[N>=5,]
dt[,.(.N,N_preg=length(which(preg==1)),N_not_preg=length(which(preg==0))), by=month][,prop_preg:=round(N_preg/N,2)]->preg_month  #[N>=5,]


#Median/quantiles of all winter seasons with at least 5 samples
preg_season[!season=="2011",][!season=="2012",] [!season=="2018",][N>=5,][,quantile(prop_preg)]
```



```{r data_wrangling_sex}
#data wrangling Sex
require(lubridate)
select(df_full, sample, date, location,season) %>% left_join(., df2 ) -> df5

df5$area <- ifelse(df5$location == "Barents Sea",1,0 )
df5$area <- replace_na(df5$area,0)
df5$area<- as.factor(df5$area )
df5$month <- month(df5$date)
df5$month <- factor(df5$month,levels = c("6","9", "10", "11", "12","1","2"))

#Scottish samples for sex ratio
clean_names(sex_Jo)->sex_Jo    
sex_Jo$date<-mdy(sex_Jo$date)
transmute(sex_Jo, sample=sample_id,year,sex,date,area=as.factor(0),month=month(date))->sex_Jo 
sex_Jo$month<-   factor(sex_Jo$month,levels = c("6","9", "10", "11", "12","1","2"))
sex_Jo$season<-  assign_season(sex_Jo$date)
sex_Jo$sex <-ifelse(sex_Jo$sex=="Male","M","F")
full_join(df5,sex_Jo)->all_sex

   
all_sex<-filter(all_sex,!sex=="NA")
```


```{r figures_sex}
require(wesanderson)
#Sex ratio across months, excl. june
all_sex %>% filter(!month==6 ) %>% 
  ggplot(aes(x = month, fill = sex)) +    
  geom_bar(position = "fill") + 
  ylab("Proportion")+
  stat_count(geom = "text", aes(label = stat(count)),position=position_fill(vjust=0.5), colour="black")+
  theme_minimal() +
  theme(legend.position = "bottom")+
scale_fill_manual(values = wes_palette("Darjeeling2", n = 2))


#plot sex ratio difference between Barents Sea and Norway, excl. june
 mutate(all_sex, area = ifelse(all_sex$area==1,"Barents Sea","Coastal Norway")) %>% 
   filter(!month ==6) %>% 
   drop_na() %>% 
   ggplot(aes(x = area, fill = sex)) +    
   geom_bar(position = "fill") + 
   ylab("proportion")+
   #stat_count(geom = "text", aes(label = stat(count)),position=position_fill(vjust=0.5), colour="black")+
   theme_minimal() +
   scale_fill_manual(values = wes_palette("Darjeeling2", n = 2)) +
   theme(legend.position = "bottom")

```


```{r sex_ratio_test}
#sex ratio deviated significantly from parity by region?
#In the Barents Sea we use a two-sided test to detect deviations from parity in both directions.  H= Parity H_alt=No Parity
binom.test(13,31, p=0.5, alternative="two.sided", conf.level = 0.95)

#Norway during winter, including data from Scotland
#We use a one-sided test because we want to test whether the amount of females is greater than males, and this increases the power of the test, which is important at this sample size. (H: the proportion is 0.5; H_alt:the proportion of success (female) is greater than 0.5).
filter(all_sex,area==0 & ! month ==6) %>% tabyl(sex) %>% adorn_totals()
binom.test(76,124, p=0.5, alternative="greater",  conf.level = 0.95)
```

```{r sex_test_2}
#Chi2 test for difference of proportions between different winter seasons, using simulated p-values to alleviate small sample size in some groups
all_sex %>%  filter(area==0 & ! month ==6) %>% tabyl(season,sex) %>% chisq.test(sim=T,B=1000)

#the difference is significant. However, this seems to be mostly driven by years in which sample sizes were low (2011/12,2017/18)
all_sex %>%  filter(area==0 & ! month ==6) %>% tabyl(season,sex) %>% adorn_totals(c('row','col'))

#throughout the winter season
all_sex  %>%  filter(area==0 & ! month ==6) %>% tabyl(month,sex) %>% filter(!M==0) %>% chisq.test(sim=T,B=1000)

setDT(all_sex) ->dt
dt[,.(.N,N_M=length(which(sex=="M")),N_F=length(which(sex=="F"))), by="month"][,ratio_M:=round(N_M/N_F,2)][]
```

```{r preg_test}
#Pregnancy rate difference between Barents Sea and Norway
chisq.test(table(c(10,30),c(2,14)),sim=T,B=1000)


###Add code from J.Kershaw who provided the poweranalysis
```

