# norwegian_whales
Analysis of sex ratio and pregnancy rates over years and through the season for humpback whales in the Barents Sea and Norway.


## Description
Preg_Sex.Rmd contains the code run for analyses, and most data. Further data files are provided in the data folder.


## Support
Contact Lisa at lisa.e.kettemer@uit.no for any questions.

## Project status
A manuscript has been submitted and is currently under review.
